package com.koval;

import com.koval.shape.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        Random rd = new Random();

        Container <Shape> shapeContainer = new Container<>();
        shapeContainer.addItem(new Circle());
        shapeContainer.addItem(new Square());
        shapeContainer.addItem(new Shape());
        shapeContainer.addItem(new Rectangle());

        List<? super Shape> shapes = new ArrayList<>();
        shapes.add(shapeContainer.getItem(rd.nextInt(shapeContainer.getLen())));
        shapes.add(shapeContainer.getItem(rd.nextInt(shapeContainer.getLen())));
        shapes.add(shapeContainer.getItem(rd.nextInt(shapeContainer.getLen())));
        shapes.remove((rd.nextInt(shapes.size())));

        logger.trace(shapes.size());

        List<? extends Shape>list = new ArrayList<>();
        List<Circle>circles = new ArrayList<>();
        List<Rectangle>rectangles = new ArrayList<>();
        list = circles;
        list.clear();
        list = rectangles;
        list.clear();

        //Everything works properly
    }
}
