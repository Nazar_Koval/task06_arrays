package com.koval;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/**
 * Class that realizes iterator for the container
 */
public class MyIterator implements Iterator<String> {
    /**
     * ArrayList to store all the strings
     */
    private ArrayList<String>data;
    /**
     * Current element`s index
     */
    private int current = 0;
    /**
     * Length of the ArrayList
     */
    private int length;

    /**
     * Main constructor
     * @param data all the strings in the container
     * @param length amount of the strings in the container
     */
    MyIterator(String[] data, int length){
        this.data = new ArrayList<>(Arrays.asList(data));
        this.length = length;
    }

    /**
     * Overridden method to check whether there are elements left
     * @return true if there are elements left or false if not
     */
    @Override
    public boolean hasNext() {
        return current < length;
    }

    /**
     * Overridden method to get next string
     * @return next string
     */
    @Override
    public String next() {
        return data.get(current++);
    }
}
