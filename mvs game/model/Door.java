package com.koval.model;

import java.util.ArrayList;
import java.util.Random;

public class Door {
    public String name;
    public int power;
    public int index;
    ArrayList<Door>doors;

    private Door(String name, int power, int index){
        this.name = name;
        this.power = power;
        this.index = index;
    }

    Door(){
        doors = generateDoors();
    }

    private String[]names = {"Dragon", "Artifact"};

    private ArrayList<Door> generateDoors(){
        Random rd = new Random();
        String tmp;
        ArrayList<Door>doorArrayList = new ArrayList<>();
        for (int i = 0; i < 10 ; i++) {
            tmp = names[rd.nextInt(names.length)];

            if(tmp.equals("Dragon")){
                Door door = new Door(tmp,rd.nextInt(5+1)+95,i+1);
                doorArrayList.add(door);
            }
            else if(tmp.equals("Artifact")){
                Door door = new Door(tmp,rd.nextInt(10+1)+70,i+1);
                doorArrayList.add(door);
            }

        }

        return doorArrayList;
    }

    public ArrayList<Door> getDoors() {
        return this.doors;
    }

    @Override
    public String toString() {
        if (this.name.equals("Dragon")) {
            return "Door #" + this.index + ": " + "Dragon with " + this.power + " xp";
        }else {
            return "Door #" + this.index + ": " + "Artifact with " + this.power + " power";
        }
    }
}
