package com.koval;

import java.util.Iterator;
import java.util.List;

public class MyIterator<T> implements Iterator<T> {

    private List<T>data;
    private int current = 0;

    MyIterator(List<T>data){
        this.data = data;
    }
    @Override
    public boolean hasNext() {
        return current < this.data.size();
    }

    @Override
    public T next() {
        return data.get(current++);
    }
}
