package com.koval;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MyDeque<T> {
    private List<T> deque;

    MyDeque(){
        this.deque = new ArrayList<>();
    }

    void addFront(T item){
        this.deque.add(0,item);
    }

    void addBack(T item){
        this.deque.add(item);
    }

    T removeFront(){
        return this.deque.remove(0);
    }

    T removeBack(){
        return this.deque.remove(deque.size()-1);
    }

    T peakFront(){
        return this.deque.get(0);
    }

    T peakBack(){
        return this.deque.get(deque.size()-1);
    }

    int size(){
        return this.deque.size();
    }

    boolean isEmpty(){
        return this.deque.size() == 0;
    }

    Iterator<T> iterator(){
        return new MyIterator<>(deque);
    }
}
