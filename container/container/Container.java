package com.koval;

import java.util.Iterator;

/**
 * Custom container`s class
 */
public class Container implements Iterable<String> {
    /**
     * Main array for the strings
     */
    private String[]data;
    /**
     * Length of the main array
     */
    private int length = 0;

    /**
     * Main container
     * @param length length of the array with data
     */
    Container(int length){
        data = new String[length];
    }

    /**
     * Method to add a particular string to the container
     * @param str string to add
     */
    void add(String str){
        data[length++] = str;
    }

    /**
     * Method to get a particular string from the container
     * @param index index of the string
     * @return found string
     */
    String get(int index){
        if(index >= length){
            throw new IndexOutOfBoundsException();
        }else {
            return data[index];
        }
    }

    /**
     * Method to get container`s size
     * @return size of the container
     */
    int size(){
        if(length == -1)
            return 0;
        else return length;
    }

    @Override
    public Iterator<String> iterator() {
        return new MyIterator(data,length);
    }
}
