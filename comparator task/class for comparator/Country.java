package com.koval;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;

public class Country implements Comparable<Country>, Comparator<Country> {

    private String name;
    private String capital;
    ArrayList<Country>countries;

    private String[]names = {"England", "Russia", "Ukraine", "Holland", "USA", "Germany", "Poland",
            "Spain", "Italy", "Canada", "Czech Republic", "Romain", "Belgium"};

    private String[]capitals = {"London", "Moscow", "Kyiv", "Amsterdam", "Washington", "Berlin",
    "Warsaw", "Madrid", "Rome", "Toronto", "Prague", "Sophia", "Brussels"};

    Country(){
        this.countries = generateCountries();
    }

    Country(String name, String capital){
        this.name = name;
        this.capital = capital;
    }

    private ArrayList<Country> generateCountries(){
        Country country;
        Random rd = new Random();
        ArrayList<Country>countries = new ArrayList<>();

        for (int i = 0; i < 13 ; i++) {
            country = new Country(names[rd.nextInt(names.length)],
                    capitals[rd.nextInt(capitals.length)]);
            countries.add(country);
        }

        return countries;
    }

    @Override
    public int compareTo(Country o) {
        return 0;
    }

    @Override
    public int compare(Country o1, Country o2) {
        return 0;
    }

    static Comparator<Country>nameComparator = new Comparator<Country>() {
        @Override
        public int compare(Country o1, Country o2) {
            return o1.name.compareTo(o2.name);
        }
    };

    static Comparator<Country>capitalComparator = new Comparator<Country>() {
        @Override
        public int compare(Country o1, Country o2) {
            return o1.capital.compareTo(o2.capital);
        }
    };

    @Override
    public String toString(){
        return  this.name + " - " + this.capital + "\n";
    }
}
