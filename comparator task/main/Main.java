package com.koval;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;

public class Main {
    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        Country country1 = new Country();
        ArrayList<Country>countries = country1.countries;

        logger.trace("Initial list: \n\n");
        for (Country c:countries) {
            logger.trace(c.toString() + " ");
        }
        logger.trace("\n");

        logger.trace("Sorted by name: \n\n");
        countries.sort(Country.nameComparator);

        for (Country c:countries) {
            logger.trace(c.toString() + " ");
        }

        Country country2 = new Country();
        countries = country2.countries;

        for (int i = 0; i <countries.size() ; i++) {
            logger.trace(countries.get(i).toString() + " ");
        }
        logger.trace("\n");

        logger.trace("Sorted by capital: \n\n");
        countries.sort(Country.capitalComparator);

        for (int i = 0; i <countries.size() ; i++) {
            logger.trace(countries.get(i).toString() + " ");
        }
        logger.trace("\n");

        int index;
        Country toFound = new Country("Holland", "Amsterdam");
        logger.trace("Country to find using binary search: " + toFound.toString() + "\n");
        index = Collections.binarySearch(countries,toFound,Country.nameComparator);
        index--;
        logger.trace("Found at index " + index + " using nameComparator in sorted by capital list");

    }
}
