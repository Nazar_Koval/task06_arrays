package com.koval;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Main class to execute program
 */
public class Main {
    /**
     * Main logger for output
     */
    private static Logger logger = LogManager.getLogger(Main.class);

    /**
     * Program  main function
     * @param args command line arguments
     */
    public static void main(String[] args) {
        Container container = new Container(10);
        container.add("string1");
        container.add("string2");
        container.add("string3");

        // For each loop
        for (String str:container) {
            logger.trace(str + " ");
        }
        logger.trace("\n");

        MyIterator iterator = (MyIterator) container.iterator();

        logger.trace(container.size()+"\n");

        // logger.trace(container.get(container.size())); Exception is thrown

        // While loop using iterator
        while (iterator.hasNext()){
            logger.trace(iterator.next() + " ");
        }
    }
}
