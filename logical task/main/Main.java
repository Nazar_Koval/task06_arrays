package com.koval;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        logger.trace("Task #1\n");
        int[]first_arr = new int[20];
        random_Array(first_arr);

        for (int i = 0; i <first_arr.length ; i++) {
            logger.trace(first_arr[i]+" ");

        }
        logger.trace("\n");

        int[]second_arr = new int[20];
        random_Array(second_arr);

        for (int i:second_arr) {
            logger.trace(i+" ");
        }
        logger.trace("\n");

        Set<Integer>first_result = intersection(first_arr,second_arr);

        for (Integer i:first_result) {
            logger.trace(i+" ");
        }

        logger.trace("\n");
        logger.trace("Task #2\n");

        random_Array(first_arr);

        random_Array(second_arr);

        for (int i:first_arr) {
            logger.trace(i+" ");

        }
        logger.trace("\n");

        for (int i:second_arr) {
            logger.trace(i+" ");
        }
        logger.trace("\n");

        Set<Integer>second_result = difference(first_arr,second_arr);

        for (Integer i:second_result) {
            logger.trace(i+" ");
        }

        logger.trace("\n");
        logger.trace("Task#3\n");

        random_Array(first_arr);

        for (int i:first_arr) {
            logger.trace(i+" ");

        }
        logger.trace("\n");

        Set<Integer>third_result = deleteClones(first_arr);
        for (Integer i:third_result) {
            logger.trace(i+" ");
        }

        logger.trace("\n");
        logger.trace("Task#4\n");

        random_Array(first_arr);

        for (int i:first_arr) {
            logger.trace(i+" ");

        }

        List<Integer>integerList = new ArrayList<>();

        for (int i = 0; i <first_arr.length-1 ; i++) {
            if(first_arr[i] != first_arr[i+1]){
                integerList.add(first_arr[i]);
            }
        }

        integerList.add(first_arr[first_arr.length-1]);
        logger.trace("\n");

        for (Integer el:integerList) {
            logger.trace(el+" ");
        }
    }

    private static Set<Integer> intersection(int[]arr1, int[]arr2){
        Set<Integer>first = new HashSet<>();
        Set<Integer>second = new HashSet<>();

        for (int i = 0; i <arr1.length ; i++) {
            first.add(arr1[i]);
        }
        for (int i = 0; i <arr2.length ; i++) {
            second.add(arr2[i]);
        }

        first.retainAll(second);

        return first;
    }

    private static Set<Integer> difference(int[]arr1, int[]arr2){
        Set<Integer>first = new HashSet<>();
        Set<Integer>second = new HashSet<>();

        for (int i1 : arr1) {
            first.add(i1);
        }
        for (int i2:arr2) {
            second.add(i2);
        }

        first.addAll(second);
        Set<Integer>intersect = new HashSet<>(first);
        intersect.retainAll(second);
        first.removeAll(intersect);

        return first;
    }

    private static Set<Integer>deleteClones(int[]arr ){
        int[]used = new int[100];
        Set<Integer>ans = new HashSet<>();

        for (int i = 0; i <arr.length ; i++) {
            used[arr[i]]++;
        }

        for (int i = 0; i <arr.length ; i++) {
            if(used[arr[i]] < 2)ans.add(arr[i]);
        }

        return ans;
    }

    private static void random_Array(int[]arr){
        Random rd = new Random();

        for (int i = 0; i <arr.length ; i++) {
            arr[i] = rd.nextInt(33);
        }
    }
}
