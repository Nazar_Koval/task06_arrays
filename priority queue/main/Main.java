package com.koval;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Iterator;

public class Main {
    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        MyPriorityQueue<Integer>myPriorityQueue = new MyPriorityQueue<>();
        myPriorityQueue.add(5);
        myPriorityQueue.add(3);
        myPriorityQueue.add(11);
        myPriorityQueue.add(4);
        Iterator<Integer>integerIterator = myPriorityQueue.iterator();
        myPriorityQueue.remove(2);
        while (integerIterator.hasNext()){
            logger.trace(integerIterator.next() + " ");
        }
        logger.trace("\n");
        logger.trace(myPriorityQueue.size() + "\n");
        logger.trace(myPriorityQueue.get(myPriorityQueue.size()-1)+"\n");
        logger.trace(myPriorityQueue.peek());
    }
}
