package com.koval;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Iterator;

public class MyPriorityQueue<T> {
    private static Logger logger = LogManager.getLogger();
    private Object[] pQueue;
    private int size;
    private int index = 0;


    private class Iter implements Iterator<T> {

        int indexNow = 0;

        public boolean hasNext() {
            return indexNow < index;
        }

        public T next() {
            if (hasNext()) {
                indexNow++;
                return (T) pQueue[indexNow - 1];
            }
            return null;
        }
    }

     MyPriorityQueue() {
        pQueue = new Object[5];
        size = 5;
    }

    int size() {
        return index;
    }

    void add(T t) {
        if (t == null)
            throw new NullPointerException();

        if (index >= size)
            sizeUp(index + 5);
        if (index == 0)
            pQueue[0] = t;
        else
            siftUpComparable(index, t);
        index++;
    }

    private void sizeUp(int capacity) {
        Object[] arrBigger = new Object[size + 5];
        size += 5;
        System.arraycopy(pQueue, 0, arrBigger, 0, pQueue.length);
        pQueue = new Object[size + 5];
        System.arraycopy(arrBigger, 0, pQueue, 0, arrBigger.length);
    }

    T get(int index1) {
        if (index1 >= index && index1 < 0) {
            logger.error("Wrong index ");
            return null;
        }
        return (T) pQueue[index1];
    }


    boolean remove(int index1) {
        if (index1 <= index && index1 >= 0) {
            Object[] arr = new Object[size];
            int count = 0;
            for (int i = 0; i < pQueue.length; i++) {
                if (i != index1) {
                    arr[i - count] = pQueue[i];
                } else {
                    count++;
                }
            }
            index--;
            pQueue = new Object[size];
            System.arraycopy(arr, 0, pQueue, 0, arr.length);
            return true;
        } else {
            logger.error("Wrong index");
            return false;
        }
    }


    void siftUpComparable(int k, T x) {
        Comparable<? super T> key = (Comparable<? super T>) x;
        while (k > 0) {
            int parent = (k - 1) >>> 1;
            Object e = pQueue[parent];
            if (key.compareTo((T) e) >= 0)
                break;
            pQueue[k] = e;
            k = parent;
        }
        pQueue[k] = key;
    }

    void siftDownComparable(int k, T x) {
        Comparable<? super T> key = (Comparable<? super T>) x;
        int half = size >>> 1;
        while (k < half) {
            int child = (k << 1) + 1;
            Object c = pQueue[child];
            int right = child + 1;
            if (right < size &&
                    ((Comparable<? super T>) c).compareTo((T) pQueue[right]) > 0)
                c = pQueue[child = right];
            if (key.compareTo((T) c) <= 0)
                break;
            pQueue[k] = c;
            k = child;
        }
        pQueue[k] = key;
    }


    T peek() {
        if (index == 0) {
            return null;
        } else {
            return (T) pQueue[0];
        }
    }

    Iterator<T> iterator() {
        return new Iter();
    }
}