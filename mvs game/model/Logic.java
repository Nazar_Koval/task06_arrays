package com.koval.model;

import java.util.ArrayList;

public class Logic implements Model {

    private Door door;

    public Logic(){
        door = new Door();
    }

    @Override
    public ArrayList<Door> generateDoors() {
        return door.doors;
    }

    @Override
    public ArrayList<Door> getDoors() {
        return door.getDoors();
    }
}
