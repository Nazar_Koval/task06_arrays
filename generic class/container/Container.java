package com.koval;
import com.koval.shape.*;
import java.util.ArrayList;
import java.util.List;

public class Container<T extends Shape> {

    private List<T> list = new ArrayList<>();
    private int len = 0;

    void addItem(T item) {
        this.list.add(item);
        this.len++;
    }

    T getItem(int index) {
        return this.list.get(index);
    }

    int getLen() {
        return this.len;
    }
}
