package com.koval;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Iterator;

public class Main {
    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        MyDeque<Integer>deque = new MyDeque<>();
        deque.addFront(4);
        deque.addBack(5);
        deque.addFront(55);
        deque.addBack(44);
        if(!deque.isEmpty()) {
            for (Iterator<Integer> iter = deque.iterator(); iter.hasNext(); ) {
                logger.trace(iter.next() + " ");
            }
        }
        logger.trace("\n");
        logger.trace(deque.peakBack() + " " + deque.peakFront() + "\n");
        logger.trace(deque.removeBack() + " " + deque.removeFront() + "\n");
        logger.trace(deque.size() + "\n");
        Iterator<Integer>integerIterator = deque.iterator();
        while (integerIterator.hasNext()){
            logger.trace(integerIterator.next() + " ");
        }
        String str = "test";
        //deque.addFront((Integer) str); Exception
    }
}
