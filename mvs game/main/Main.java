package com.koval;

import com.koval.view.*;

public class Main {

    public static void main(String[] args) {
        new View().interact();
    }
}
